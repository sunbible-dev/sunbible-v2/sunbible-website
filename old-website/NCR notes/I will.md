#  I will verses from Psalm (I Will. -- Will You?)

" I will love thee, O Lord, my strength. "
- Psalm 18:1

" I will declare thy name unto my brethren: in the midst of the congregation will I praise thee. "
- Psalm 22:22


" I will confess my transgressions unto the Lord "
- Psalm 32:5b

" For I will declare mine iniquity; I will be sorry for my sin. "
- Psalm 38:18


" What time I am afraid, I will trust in thee . "
- Psalm 56:3

" I will lift up mine eyes unto the hills, from whence cometh my help. "
- Psalm 121:1


" I will sing of mercy and judgment: unto thee, O Lord, will I sing.
2 I willl behave myself wisely in a perfect way. O when wilt thou come unto me? I will walk within my house with a perfect heart.
3 I will set no wicked thing before mine eyes: I hate the work of them that turn aside; it shall not cleave to me.
4 A froward heart shall depart from me: I will not know a wicked person.
5 Whoso privily slandereth his neighbour, him will I cut off: him that hath an high look and a proud heart will not I suffer.
6 Mine eyes shall be upon the faithful of the land, that they may dwell with me: he that walketh in a perfect way, he shall serve me.
7 He that worketh deceit shall not dwell within my house: he that telleth lies shall not tarry in my sight.
8 I will early destroy all the wicked of the land; that I may cut off all wicked doers from the city of the Lord. "
- Psalm 101


" I love the Lord, because he hath heard my voice and my supplications.
2 Because he hath inclined his ear unto me, therefore will I call upon him as long as I live.
3 The sorrows of death compassed me, and the pains of hell gat hold upon me: I found trouble and sorrow.
4 Then called I upon the name of the Lord; O Lord, I beseech thee, deliver my soul.
5 Gracious is the Lord, and righteous; yea, our God is merciful.
6 The Lord preserveth the simple: I was brought low, and he helped me.
7 Return unto thy rest, O my soul; for the Lord hath dealt bountifully with thee.
8 For thou hast delivered my soul from death, mine eyes from tears, and my feet from falling.
9 I will walk before the Lord in the land of the living.
10 I believed, therefore have I spoken: I was greatly afflicted:
11 I said in my haste, All men are liars.
12 What shall I render unto the Lord for all his benefits toward me?
13 I will take the cup of salvation, and call upon the name of the Lord.
14 I will pay my vows unto the Lord now in the presence of all his people.
15 Precious in the sight of the Lord is the death of his saints.
16 O Lord, truly I am thy servant; I am thy servant, and the son of thine handmaid: thou hast loosed my bonds.
17 I will offer to thee the sacrifice of thanksgiving, and will call upon the name of the Lord.
18 I will pay my vows unto the Lord now in the presence of all his people.
19 In the courts of the Lord's house, in the midst of thee, O Jerusalem. Praise ye the Lord. "
- Psalm 116


"I will praise thee with uprightness of heart, when I shall have learned thy righteous judgments.

I will keep thy statutes: O forsake me not utterly.

I will meditate in thy precepts, and have respect unto thy ways.

I will delight myself in thy statutes: I will not forget thy word.

I will run the way of thy commandments, when thou shalt enlarge my heart.

And I will walk at liberty: for I seek thy precepts.
I will speak of thy testimonies also before kings, and will not be ashamed.
And I will delight myself in thy commandments, which I have loved.
My hands also will I lift up unto thy commandments, which I have loved; and I will meditate in thy statutes.

At midnight I will rise to give thanks unto thee because of thy righteous judgments.

The proud have forged a lie against me: but I will keep thy precepts with my whole heart.

Let the proud be ashamed; for they dealt perversely with me without a cause: but I will meditate in thy precepts.

I will never forget thy precepts: for with them thou hast quickened me.

The wicked have waited for me to destroy me: but I will consider thy testimonies.

I have sworn, and I will perform it, that I will keep thy righteous judgments.

Depart from me, ye evildoers: for I will keep the commandments of my God.

Hold thou me up, and I shall be safe: and I will have respect unto thy statutes continually.

I cried with my whole heart; hear me, O LORD: I will keep thy statutes."


- Psalm 119:7, 8, 15, 16, 32, 45-48, 62, 69, 78, 93, 95, 106, 115, 117, 145

" For my brethren and companions' sakes, I will now say, Peace be within thee. "

" Because of the house of the Lord our God I will seek thy good. "
- Psalm 122:8-9

