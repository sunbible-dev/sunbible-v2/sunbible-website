# Memorize


<div id="BibleMemorizationGuide">
  Romans 8:35-39 are really nice verses to memorize. <b>Hints for memorizing verse 38-39:</b>
  In these verses Paul mentions 10 different things that he says can't separate us from the love of God.
  You can point at a different finger for each word. You can also write the first letter of those ten words on your finger.
  For a video on how to memorize these verse <a href="https://youtu.be/HXA-d-Dqsok" style="color: blue">click here</a>
</div>

<div id="BibleMemorizationGuide">
  Ephesians 4:20-31 is a great passage to memorize.
  For memory tips and more visit:
  <a href="https://thesunshining.weebly.com/sunshine-on-bible-memorization.html">
  SunShine on Bible Memorization
  </a>
  </div>

  <div id="BibleMemorizationGuide">
  Ephesians 6:10-18 is a great passage to memorize.
  Find great memorization tips and stuff at:
  <a href="https://thesunshining.weebly.com/sunshine-on-bible-memorization.html">SunShinine On Bible Memorization</a>
  </div>