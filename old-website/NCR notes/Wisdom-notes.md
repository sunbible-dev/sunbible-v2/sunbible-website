# Wisdom


"For the wisdom of this world is foolishness with God. For it is written, He taketh the wise in their own craftiness.
Because the foolishness of God is wiser than men; and the weakness of God is stronger than men." - 1 Corinthians 3:19;1:25

If any of you lack wisdom, let him ask of God, that giveth to all men liberally, and upbraideth not; and it shall be given him. - James 1:5