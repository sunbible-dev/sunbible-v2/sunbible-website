# Job


<div id="NCRnote" style="width: 700px;">
<b>Intro to book of Job:</b><br/>
The book of Job is one of my favorite old testament books. I do not know if the story that is told in the book is a true story,
but I do know that the book contains much truth. The book of Job is kind of allegorical, and is classified with the poetry books of the bible.<br/>
The book of Job contains quite a few good questions to help people think. And if you know me. You know that I like questions. As you read through the book of Job
 I encourage you to spend some time reading and thinking over some of the questions.<br/>
 If you like questions I encourage you to visit my website: <a href="https://sites.google.com/view/questionstohelp/">Questions To Help</a>
</div>