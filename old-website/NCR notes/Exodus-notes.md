# Exouds Notes:


<div class="The10Pintro">
<h2>The 10 Plagues of Egypt - Exodus 7-12</h2>
<div>
  <p> - The 10 plagues were attacks against the gods of Egypt.</p>
<p> - They where meant to show the power of God.</p>
<p> - I also think the plagues were to show His people His power because when they were in Egypt they were
  surrounded by a suppositious nation that served many gods.</p>
</div>
</div>


<div class="A10Pnote">
  <b>1. Water to Blood</b> - Exodus 7:14-25<br/>
<b>Description:</b> All the water turned to blood. The main water source for Egypt was the Nile River. I don't know how much the Egyptians worshiped the Nile. I feel as if turning all their water to blood was an attack against their belief/faith in the Nile.
<br/>
<b>Duration:</b> 7 days<br/>
<b>Duplicatable:</b> yes, the magicians of Egypt managed to duplicate it, but not to the same extent.
<br/>
<b>Did this plague affect God's people?</b> It is not very clear in the text whether or not this plague affected God's people. But it is most likely it affected them in some way or another because all the water was turned to blood.
<br/>
<b>What did Moses have to do?</b> Moses had to hold his stick out over the water or strike the water.
<br/>
<b>What did God do?</b> God turned all the water into blood.
</div>


 <div class="A10Pnote E234">
     <b>2. Frogs</b> - Exodus 8:1-15<br/>
<b>Description:</b> Land was filled with frogs, they came up out of the Nile. I don't know how much the Egyptians worshiped the frogs, but I feel as if the plague of the frogs was an attack against how the Egyptians worshiped the frogs.
<br/>
<b>Duplicatable:</b> yes, the magicians of Egypt managed to duplicate it, but not to the same extent.
<br/>
<b>Did this plague affect God's people?</b> Probably to a certain extent.
<br/>
<b>What did Moses have to do?</b>  Tell Aron to stretch forth his hands over the streams and  Rivers.
<br/>
<b>What did God do?</b> God caused frogs to come up out of the waters.
    </div>

    <div class="A10Pnote E234">
     <b>3. Lice</b> - Exodus 8:16-19<br/>
     <b>Description:</b> All the dust of the land turned to lice all over Egypt.<br/>
     <b>Duplicatable</b>: No. The magicians tried to, but couldn't. They told Pharaoh that it was the hand of God.
     <br/>
     <b>Did this plague affect God's people?</b>  Probably to a certain extent.
     <br/>
     <b>What did Aaron have to do?</b> Stretch out his rod and hit the dust of the land.
     <br/>
     <b>What did God do?</b> Turned the dust to lice.
    </div>

    <div class="A10Pnote E234">
      <b>4. Flies</b> - Exodus 8:20-32<br/>
<b>Description:</b> Land filled with flies.<br/>
<b>Duplicatable:</b> This is the first one that does not mention that the magicians trying to do the same thing.
<br/>
<b>Did this plague affect God's people?</b> This is also the first one where it clearly says this plague was just for the Egyptians.
<br/>
<b>What did Moses have to do?</b> Just go to Pharaoh again and demand that he let God's people go.
<br/>
<b>What did God do?</b> Sent flies into the land.
    </div>


    
<div class="A10Pnote N567">
  <b>5. Grievous Murrain</b> - KJV (Plague on all the livestock in the field - NIV) - Exodus 9:1-7<br/>
  <b>Description:</b> Death to all the animals of the Egyptians that were in the fields. (I think the Egyptians worshiped cattle to some extent.)
    <b>Duplicatable:</b> It seems that after the plague of the lice, the magicians stopped trying to duplicate the plagues.
    <b>Did this plague affect God's people?</b> No, the cattle of the children of Israel did not die.
    <b>What did Moses have to do?</b> Just go to Pharaoh again and demand that he let God's people go.
    <b>What did God do?</b> Killed all the cattle of Egypt.
</div>

<div class="A10Pnote N567">
  <b>6. Boils</b> - Exodus 9:8-12<br/>
  <b>Description:</b> Every human and animal was covered with sores.
  <br/>
  <b>Duplicatable:</b> The magicians could not stand before Moses. They were to sore. haha.
  <br/>
  <b>Did this plague affect God's people?</b> It does not say.
  <br/>
  <b>What did Moses have to do?</b> He and Aaron had to take a handful of ashes and sprinkle it toward the heaven in the sight of Pharaoh.
  <br/>
  <b>What did God do?</b> Turned it in sores that covered all the Egyptians and their animals.
</div>

<div class="A10Pnote N567">
  <b>7. Pestilence</b> (hail, thunder, rain) - Exodus 9:13-35<br/>
  <b>Description:</b> Hail, Rain, Thunder.
  All the crops were ruined.
  And many people may have been killed, and animals too that were not in a building.
  <br/>
  <b>Did this plague affect God's people?</b> No.<br/>
  <b>What did Moses have to do?</b> Wake up early and go to Pharaoh, and raise his hands up to heaven.<br/>
  <b>What did God do?</b> Made it hail, thunder, and rain.
</div>



    <div class="A10Pnote">
      <b>8. Locust</b> - Exodus 10:1-20 <br/>
      <b>Description:</b> Land was filled with locust. They ate anything green that was left. <br/>
      <b>Did this plague affect God's people?</b> Probably to a certain extent. But we have to remember that they were leaving Egypt, so it didn't bother them that all the crops where ruined.
      <br/>
      <b>What did Moses have to do?</b> Go to Pharaoh again, ask that he let God's people go.
      <br/>
      <b>What did God do?</b> Brought locust to Egypt.
    </div>

    <div class="A10Pnote">
      <b>9. Darkness</b> - Exodus 10:21-23 <br/>
      <b>Description:</b> The land where the Egyptians lived was filled with darkness that could be felt. <br/>
      <b>Duration:</b> This darkness lasted 3 days. <br/>
      <b>Did this plague affect God's people?</b> No, there was light where they were. <br/>
      <b>What did Moses have to do?</b> Stretch his hands to heaven. <br/>
      <b>What did God do?</b> God made darkness that could be felt and made it last for 3 days.
    </div>



    <div class="A10Pnote">
  <b>10. Death of the firstborn</b> - Exodus 10:24-12:51 <br/>
  <b>Description:</b> All the firstborn died. <br/>
  <b>Did this plague affect God's people?</b> No, because they marked their doorpost with the blood of a lamb.<br/>
  <b>What did Moses have to do?</b>  Moses and the children of Israel marked their doorpost with blood so God would not kill their firstborn children.<br/>
  <b>What did God do?</b> Killed the firstborn children.<br/>
</div>


<div class="A10Pnote">
  The above notes about the 10 plagues where created June, 2020 when I studied through the book of Exodus.<br/>
  And I used those notes for my show: "<a href="https://www.youtube.com/playlist?list=PLhdlOfKFUjhhUgVnHmrtko3B5GyIk03qG">My Sister Named Faith</a>".
  In the episode about Moses Part 1.
</div>





<div id="The10C" style="text-align: center">
      <h1 id="The10C">The 10 Commandments</h1>
      <h4>Exodus 20:1-17</h4>
    </div>
    <div id="NCRnote"
      style="background-color: gray; width: 150px; float: right; padding: 10px; border: 5px solid gold;">
      The 10 Commandments<br />
      1. Verse 3<br />
      2. Verses 4-6<br />
      3. Verse 7<br />
      4. Verses 8-11<br />
      5. Verse 12<br />
      6. Verse 13<br />
      7. Verses 14<br />
      8. Verse 15<br />
      9. Verse 16<br />
      10. Verse 17<br />
      -------<br />
      Can you find the first commandment with promise?<br />

    </div>