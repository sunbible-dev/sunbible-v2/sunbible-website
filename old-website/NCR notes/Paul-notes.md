# Paul

Some Fun Facts:
Paul wrote 13 epistles or letters in the new testament.
He is the most well known missionary in the new testament.
He wrote 4 letters in Prison.: Ephesians, Philippians, Colossians, and Philemon




<div id="NCRnote">
  In Ephesians 6:1-3 Paul says that "honour your father and mother" is the first commandment with promise.
  Do you know what that promise is?
  You may like to look at the <a href="Exodus.html#The10C">10 Commandments</a> in Exodus 20.
  </div>


  <div id="NCRnote">
  Ephesians 6:10-18 is the theme passage for my book "Ben and His Shadow Buddy".
  It is a passage on spiritual warfare and the armour of God.
   It is a great passage to study and memorize.
   <a href="https://sites.google.com/view/benandhisshadowbuddy/home">Ben And His Shadow Buddy.com</a>
  </div>